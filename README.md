# Leica MiuiCamera for Poco X3 NFC

### Cloning :
- Clone this repo in vendor/xiaomi/miuicamera in your working directory by :
```
git clone https://gitlab.com/TheStrechh/android_vendor_xiaomi_miuicamera -b 13 vendor/xiaomi/miuicamera
```

Make these changes in **device.mk**

```
# MIUI Camera
$(call inherit-product-if-exists, vendor/xiaomi/miuicamera/config.mk)
```

## Credits

### * crDroid Team
### * Deleted Account
